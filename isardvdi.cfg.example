# Isard main config v1.1.1

# ------ Docker Compose ------------------------------------------------------

## This configuration should generate docker-compose file for the following
## flavour
## Values: (first value is the default one)
##  - all-in-one: all services except toolbox in one docker-compose file
##  - hypervisor: hypervisor and related service to access to their desktops
##  - hypervisor-standalone: hypervisor without related services
##  - video-standalone: services to access to desktops
##  - toolbox: service to manage desktops disks
##  - toolbox-base: base image for toolbox, just to improve CI/CD build
##  - web: services to manage desktops
#FLAVOUR=all-in-one

## This configuration should generate docker-compose file for the following
## usage
## Values: (first value is the default one)
##  - production: docker-compose file without build section
##      ready to pull and up
##      (see also: https://github.com/docker/compose/issues/7873)
##  - build: ready to build with build section
##  - devel: host source code are mounted into docker volumes
#USAGE=production

## Should stats service will be included
## Values: (first value is the default one)
##  - true: add stats service
##  - false: doesn't add stats sercice
#ENABLE_STATS=true

# ------ Domain --------------------------------------------------------------
## This server main public domain/ip.
## NOTE: In infrastructure hypervisors should be the reacheable ip/dns from 
##       isard-engine
DOMAIN=localhost

# ------ Admin password ------------------------------------------------------
## Initial WEB admin user password. Used also in grafana, influxdb
## and authenticated backend paths (/debug/...)
WEBAPP_ADMIN_PWD=IsardVDI

# ------ Frontend Administration Button --------------------------------------
## Will show/hide Administration menu button for users of role User.
#FRONTEND_SHOW_ADMIN_BTN=True

# ------ Secrets -------------------------------------------------------------
## Generate your own SECRETS!
## openssl rand -base64 32
WEBAPP_SESSION_SECRET=xq0Z3MP5ujxrQxtMGxgPiijH9xpuxkyP04R6At/V+g4=
API_ISARDVDI_SECRET=kpWpdF0NtI2XCEfzMp36hdSV9S42E7axS8D5TvP9c0A=
INFLUXDB_ADMIN_TOKEN_SECRET=9eFW/Qi29hL3hFGUP8wIGH89XKCH8s1k0il44GCRF2g=
API_HYPERVISORS_SECRET=B5/bUEUzIC+AjNQRmFh3vxR3VeIKirwdeL/xuHPVO+E=

# ------ Letsencrypt certificate ---------------------------------------------
## You can use your own certificates if you concatenate into 
## /opt/isard/certs/default/chain.pem
## You can let isard generate autosigned certs (not recommended as
## html5 viewers will not work.
## Or you can let isard generate letsencrypt certs for your domain.
## For this option to work be sure the DOMAIN points to this IP.
## To avoid using letsencrypt let this variable commented.
## The email will be used by letsencrypt to notify you expirations
## although the renovation will be automatic.
#LETSENCRYPT_EMAIL=

# ------ Authentication ------------------------------------------------------

## Local
### Local authentication against isard database
#AUTHENTICATION_AUTHENTICATION_LOCAL_ENABLED=true

## LDAP
#AUTHENTICATION_AUTHENTICATION_LDAP_ENABLED=false
#AUTHENTICATION_AUTHENTICATION_LDAP_PROTOCOL=ldap
#AUTHENTICATION_AUTHENTICATION_LDAP_HOST=
#AUTHENTICATION_AUTHENTICATION_LDAP_PORT=389

### Credentials used for querying the LDAP
#AUTHENTICATION_AUTHENTICATION_LDAP_BIND_DN=
#AUTHENTICATION_AUTHENTICATION_LDAP_PASSWORD=

### Base Search is the DN that all the users share, e.g. ou=people,dc=example,dc=com
#AUTHENTICATION_AUTHENTICATION_LDAP_BASE_SEARCH=
### Filter is the actual filter used to search users. The '%s' represents the user that is sent through the form
### More information: https://confluence.atlassian.com/kb/how-to-write-ldap-search-filters-792496933.html
#AUTHENTICATION_AUTHENTICATION_LDAP_FILTER="(&(objectClass=person)(uid=%s))"

### These are the fields that the LDAP search responds. For example, in some installations, the field for the email is called 'mail'
### Then, a regex is applied to this field, in case we needed to filter inside a LDAP field. By default it collects the whole field
#AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_UID=
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_UID=.*
#AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_USERNAME=
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_USERNAME=.*
#AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_NAME=
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_NAME=.*
#AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_EMAIL=
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_EMAIL=.*
#AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_PHOTO=
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_PHOTO=.*

## Google
### Create your Google OAUTH credentials at https://console.developers.google.com/apis/credentials
### Authorized redirect URIs: https://domain.tld/authentication/callback
### Change AUTHENTICATION_AUTHENTICATION_HOST to domain.tld
#AUTHENTICATION_AUTHENTICATION_GOOGLE_ENABLED=false
#AUTHENTICATION_AUTHENTICATION_GOOGLE_CLIENT_ID=id
#AUTHENTICATION_AUTHENTICATION_GOOGLE_CLIENT_SECRET=secret




##################################################################
##################################################################
## DO NOT EDIT FROM HERE UNLESS YOU KNOW WHAT YOU ARE DOING !!! ##
##################################################################
##################################################################

# ------ Docker images prefix ------------------------------------------------
## Image prefix that could include registry and repository
DOCKER_IMAGE_PREFIX=registry.gitlab.com/isard/isardvdi/

# ------ Docker images tags --------------------------------------------------
## Image tag that could be tags or branches from the git repository
## Used for doing docker-compose pull
DOCKER_IMAGE_TAG=main

# ------ Logs ----------------------------------------------------------------
LOG_LEVEL=INFO

# ------ alerts --------------------------------------------------------------
## Telegram
### TOKEN: https://core.telegram.org/bots#6-botfather
#TELEGRAM_BOT_TOKEN=
### CHAT_ID: Add your bot to a group and get the chat_id from 
### https://api.telegram.org/bot<YourBOTToken>/getUpdates response
#TELEGRAM_BOT_CHAT_ID=

##################################################################
## INFRASTRUCTURE PARAMETERS. Used on remote hypervisors        ##
##################################################################
## Remote hypervisors can be:
## docker-compose.hypervisor.yml: Will have an isard-video
## docker-compose.hypervisor-standalone.yml: Will get video on 
## the VIDEO_HYPERVISOR_PORTS from the VIDEO_DOMAIN isard-video host.

# ------ Hypervisor Identifier -----------------------------------------------
## Set it to a unique name for hypervisor
#HYPER_ID=isard-hypervisor

# ------ Database host -------------------------------------------------------
## Where is the database reacheable?
## Not needed for remote hypervisors, only to split main web install.
#RETHINKDB_HOST=isard-db
#RETHINKDB_PORT=28015
#RETHINKDB_DB=isard

# ------ Authentication host -------------------------------------------------

# Authentication host
## Where is isard-authentication reacheable from clients browser?
## Not needed for remote hypervisors, only to split main web install.
#AUTHENTICATION_AUTHENTICATION_HOST=$DOMAIN
#AUTHENTICATION_DB_HOST=isard-db

# ------ Api host ------------------------------------------------------------
## Where can this host reach the isard-api host?
## Need to be set for flavours:
##   - hypervisor
##   - hypervisor-standalone
#API_DOMAIN=isard-api

# ------ Static nginx host ----------------------------------------------------
## Where the clients browsers will load static when connecting to this host 
## html5 video?
## Need to be set for flavours:
##   - hypervisor
##   - hypervisor-standalone
#STATIC_DOMAIN=$DOMAIN

# ------ Vpn host ------------------------------------------------------------
## Where can this host reach the isard-vpn host?
## Need to be set for flavours:
##   - hypervisor
##   - hypervisor-standalone
#VPN_DOMAIN=isard-vpn

## The vpn mtu will allow for infrastructure connection (allow it in your
## switches)
## If using Internet to connect remote hypers this should be lowered.
## https://keremerkan.net/posts/wireguard-mtu-fixes/
## https://mail.openvswitch.org/pipermail/ovs-discuss/2018-June/046932.html
#VPN_MTU=1600

# ------ Video proxy host ----------------------------------------------------
## Where will the client browser reach isard-video to this host?
## Need to be set for flavours: 
##   - video-standalone (to generate letsencrypt certs)
##   - hypervisor (to generate letsencrypt certs and set up hyper in db)
##   - hypervisor-standalone ( to set up hyper in db)
#VIDEO_DOMAIN=$DOMAIN

# ------ Video external NAT ports --------------------------------------------
## Where the users browsers will connect to get the video stream for
## their guests started in this hypervisor? (outside NAT ports)
#VIEWER_SPICE=80
#VIEWER_BROWSER=443

# ------ Video proxy ACL -----------------------------------------------------
## Is this host hosting the isard-video for other hypervisor-standalone
## servers? Then allow here only the hostnames for those hypervisors as
## seen from this host with comma (,) delimiters:
#VIDEO_HYPERVISOR_HOSTNAMES=isard-hypervisor
#VIDEO_HYPERVISOR_PORTS=5900-6899

# ------ Docker networking ---------------------------------------------------
## Assign a docker /24 network. The host part will be set by system
## You should avoid setting a network that exists in your infrastructure
## or in isard configuration. Set only the /24 part!
#DOCKER_NET=172.31.255

# ------ Guests networking ---------------------------------------------------
## All the sub networks needed for infrastructure wireguard will
## fall within this sub networks
WG_MAIN_NET=10.0.0.0/14

## Users at home will get a unique /32 IP from this range.
## Set a network that will allow as many clients as you will have.
WG_USERS_NET=10.0.0.0/16

## UDP port for users at home to connect wireguard to this server.
WG_USERS_PORT=443

## Only in infrastructure this will be used by remote hypers to 
## send wireguard guests network to the main Isard. 
WG_HYPERS_NET=10.1.0.0/24

## UDP port for remote hypervisors to connect to this server.
WG_HYPERS_PORT=4443

## This is the main range to be used by wireguard interface in
## guests in your system. Will be subdivided in smaller ranges
## for each hypervisor.
WG_GUESTS_NETS=10.2.0.0/16

## 23: 512 GUESTS -2
## 24: 256 GUESTS -2
## Each hypervisor will get one subnet from WG_GUESTS_NETS
## Last network will be subdivided /29 to connect wireguard hyper
## clients (isard-vpnc) to the wireguard server (isard-vpn)
WG_GUESTS_DHCP_MASK=23
## This sets a reserved dhcp range if you want.
WG_GUESTS_RESERVED_HOSTS=20

# ------ Trunk port & vlans --------------------------------------------------
## Uncomment to map host interface name inside hypervisor container.
## If static vlans are commented then hypervisor will initiate an 
## auto-discovery process. The discovery process will last for 260
## seconds and this will delay the hypervisor from being available.
## So it is recommended to set also the static vlans.
## Note: It will create VlanXXX automatically on webapp. You need to
##       assign who is allowed to use this VlanXXX interfaces.
#HYPERVISOR_HOST_TRUNK_INTERFACE=

## This environment variable depends on previous one. When setting
## vlans number comma separated it will disable auto-discovery and
## fix this as forced vlans inside hypervisor.
#HYPERVISOR_STATIC_VLANS=

## NOTE: The interface name should not be changed from defaults.
##       Newer kernels use cgroups2 that doens't work. To get the
##       old cgroups add to /etc/default/grub:
##        GRUB_CMDLINE_LINUX="systemd.unified_cgroup_hierarchy=0"
##       and update-grub, and reboot.

# ------ Override default paths ----------------------------------------------
#PATH_VPN_CERTS=/opt/isard-local/wireguard/certs

# ------ Override hosts in isard-portal haproxy ------------------------------
#WEBAPP_HOST=isard-webapp
#GRAFANA_HOST=isard-grafana
#INFLUXDB_HOST=isard-influxdb


# ------ Stats ---------------------------------------------------------------
## If you need to monitor a remote host be sure to add it's ssh keys
## manually from inside the isard-stats container.
## doker exec -t isard-stats ssh-keygen USER@HYPERHOST
#STATS_HYP_HOSTNAME=isard-hypervisor
#STATS_HYP_USER=root
#STATS_HYP_PORT=2022

#STATS_SEND_TO_INFLUX=true
#STATS_INFLUX_PORT=8086
#STATS_INFLUX_BUCKET=isardvdi
#STATS_INFLUX_ORG=isardvdi
#STATS_TYPE_CONN_SS=ssh|docker
